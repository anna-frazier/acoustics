function [freqVec, crossSpec] = crossSpectrum(timeSeries1, timeSeries2, dt, outputType)

% PURPOSE:
% This function outputs the cross-spectral density, Gxy, between two
% inputted time series

% INPUTS: 
%   timeSeries1 = First time series (x)
%   timeSeries2 = Second time series (y)
%   dt = time increment between samples [s]

% OUTPUTS: 
%   freqVec = vector of frequencies for avgGxx [Hz]
%   avgGxx = average single-sided spectral density [WU^2/Hz]

% WRITTEN BY ANNA FRAZIER, OCTOBER 2021

% Check that the length of the two time series is equal: 
if length(timeSeries1) ~= length(timeSeries2)
    error('The length of the two time series is not equal. Fix and try again!')
end

N = length(timeSeries1);
T = N*dt; 
fs = 1/dt; 

[freqVec, linSpec1] = linearSpectrum(timeSeries1, dt, 0);
[freqVec, linSpec2] = linearSpectrum(timeSeries2, dt, 0);

crossSpec = conj(linSpec1).*linSpec2*(1/T); 

if outputType == 'double'
    for i = 1:length(freqVec)
        if freqVec(i)>(fs/2)
            freqVec(i) = freqVec(i)-fs; 
        end
    end
end 

if outputType == 'single'
    for i = 1:length(crossSpec)
        if i>1 & i<(N/2+1)
            crossSpec(i) = crossSpec(i)*2; 
        end
    end
    crossSpec = crossSpec(1:(N/2+1)); 
    freqVec = freqVec(1:(N/2+1));
end

end
