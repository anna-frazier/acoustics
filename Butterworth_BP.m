function [bb, aa] = Butterworth_BP(N, f1_actual, f2_actual,fs);
%
%     This routine generates the 'filter' coefficients for IIR implementation
%  of a Butterworth band-pass filter of order, N, with 3-dB frequencies, f1 to f2,
%  and for a sampling rate of fs.  The frequencies entered are 'warped'
%  internal to the function so that the z-transform filter has the proper
%  frequencies; hence, the labels '_actual' on the inputs.  Enter the
%  actual frequencies you intend to be the 3-dB points.
%
%  Usage:
%
%     To produce a bandpass filter of order 2N with lower 3-dB edge at f1
%   and upper 3-dB edge at f2 (for use with data sampled at fs):
%
%     [bb, aa]  = Butterworth_BP(N, f1, f2, fs);
%     yfiltered = filter(bb, aa, yunfiltered);
%
if f2_actual < f1_actual;  %  Ensure that f2 > f1
    f1_temp = f1_actual;  f1_actual = f2_actual;  f2_actual = f1_temp;
end
%
if f2_actual > 0.99*fs/2;  %  Check upper frequency against sampling rate
    error('Sampling rate is too low for requested filter');
end
%   Warp frequencies to account for bilinear transformation
f1 = fs*tan(pi*f1_actual/fs)/pi;
f2 = fs*tan(pi*f2_actual/fs)/pi;
%   Calculate geometric-mean center frequency
fctr = sqrt(f1*f2);
%
%   Angular location of poles:
theta = pi/N;
angs = ((pi+theta)/2):theta:(3*pi/2);
%
%
%  BAND-PASS Filter
%
w0 = 2*pi*fctr; w02 = w0^2;
%  Radial location of poles: (Note the relationship to the bandpass filter
%  bandwidth, not the center frequency.)
w_crit = 2*pi*(f2-f1);
%  The poles:
roots = w_crit*exp(j*angs);
%
%  Start with the s-domain coefficients for the low-pass filter (but with
%  the adjusted radial location of the poles)...
dena = real(poly(roots));
mplus1 = length(dena);
numa = dena(mplus1);
%
%    Modify coefficient vectors for bandpass filter.  In the s-domain this
%  involves replacing s by the quantity (s^2 + w0^2)/s.  (This doubles the
%  order of the denominator polynomial.)  As in the high-pass filter, we
%  multiply numerator and denominator by s^m to make the denominator a
%  proper polynomial.
nums = [numa, zeros(1,mplus1-1)];  %  ...equivalent to multiplying num by s^m
%
%    To find the polynomial coefficients for the new denominator, we must
%  expand the various powers of (s^2 + w0^2).  One way to do that is to
%  express that quantity as a polynomial with two roots, s = jw0 and s =
%  -jw0.  The routine below starts with an empty coefficient vector of the
%  ultimate length (2m+1) and fills it by adding in each subsequent term.
%  Notice that the terms in the denominator (after multiplying by s^m) have
%  following sequence of s expressions:  s^m; (s^2 + w0^2) * s^(m-1);
%  (s^2 + w0^2)^2 * s^(m-2) + ... + (s^2 + w0^2)^m.  After expansion, these
%  factors have the following powers of s: m; m-1,m,m+1; m-2,m-1,m,m+1,m+2;
%  and so forth.  This expanding range of powers of s is represented in the
%  code below with the indices, m1 and m2.  The index m1 also tracks the
%  appropriate coefficient from the 'old' denominator.
%
%  Initialize quantities:
dens = zeros(1,2*mplus1-1); tempd = dens;
m1 = mplus1; m2 = m1;
rootv0 = [-j*w0  j*w0];  rootv = rootv0;  % one rootv0 for each s^2+w0^2 factor
%
%  Put first term's coefficient into the new denominator coefficient
%  vector:
dens(m1) = dena(mplus1);
%
%  Loop through remaining coefficients
for ii=1:(mplus1-1)
    m1 = m1 - 1; m2 = m2 + 1;
    %  Find coefficients for appropriate power of (s^2 + w0^2)
    basic_coef = poly(rootv);
    %  Put those coefficients into tempd in locations that take care of the
    %  appropriate power of s
    tempd(m1:m2) = basic_coef;
    %  Multiply by the proper original coefficient and sum into new vector
    dens = dens + dena(m1)*tempd;
    %  Account for next higher power of (s^2 + w0^2) by adding another pair
    %  of roots
    rootv = [rootv, rootv0];
end
%
%  Find the z-domain coefficients from the s-domain coefficients via the
%  bilinear transformation:
[bb,aa] = bilinear_xform(nums,dens,fs);
end
