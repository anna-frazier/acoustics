%Code to generate pure tones for SoundNife testing 

f = 1000; 
fs = 96000; 
dt = 1/fs; 

d = 30; %10 second tone
t = 0:dt:d; 

x = sin(2*pi*f*t); 

ramp_duration = 0.5; %seconds
ramp_length = ramp_duration * fs; %number of samples 

ramp = [1:ramp_length]/ramp_length; 

x_ramp(1:ramp_length) = x(1:ramp_length).*ramp; 

x = int16(32767.*x); %Converts to correct size int type
x_ramp = int16(32767.*x_ramp);

xx = [x(:)];
%xx = [x(:), x(:)]; 
%xx_ramp = [x_ramp(:), x_ramp(:)]; 


audiowrite('1000Hz.wav', xx, fs)
%audiowrite('test_ramp.wav', xx_ramp, fs)