function [freqVec, linSpec] = linearSpectrum(timeSeries, dt, fig)

% PURPOSE:
% This function outputs the linear spectrum of a given time series.

% INPUTS: 
%   timeSeries = vector of data [WU]
%   dt = time increment between samples [s]
%   fig = option to plot linear spectrum (will plot if equal to 1)

% OUTPUTS: 
%   freqVec = frequency vector of linear spectrum [Hz]
%   linSpec = linear spectrum [WU*s]

% WRITTEN BY ANNA FRAZIER, SEPTEMBER 2021

linSpec = fft(timeSeries)*dt;
N = length(timeSeries); 
df = 1/N/dt; 
freqVec = [(0:N-1).*df]';

if fig == 1
    figure('Name', 'Linear Spectrum')
    plot(freqVec, abs(linSpec))
    xlabel('Frequency [Hz]', 'FontSize', 12)
    ylabel('Linear Spectrum [WU/Hz]', 'FontSize', 12)
    title('Linear Spectrum of Time Series Data')
    grid on 
    grid minor
end

end

