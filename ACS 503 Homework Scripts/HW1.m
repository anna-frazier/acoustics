%Anna Frazier 
%ACS 503 - Signal Analysis 
%Homework 1 - The Linear Spectrum and the FFT

clear variables 
close all 

%% Create time series
%randNums = [6 2 9 11 5 3 9 16]; randNums = randNums.' % [Pa]
randNums = [2 -2 2 -2 2 -2 2 -2]; randNums = randNums.'

dt = 0.01; % time increment between samples, [s]
fs = 1/dt; %Sampling frequency (Hz)
N = length(randNums); %number of samples in series

timeVec = (0:N-1)*dt; timeVec = timeVec.' %Making it a column vector

%% Find the Linear Spectrum
linSpec = fft(randNums)*dt
df = 1/N/dt; % frequency interval of linear spectrum

%% Sine Function Time Series
A = 0.5;                   % Amplitude, [Pa]
f0 = 252;                    % Frequency, [Hz]
fs = 128;                  % Sample Rate, [Hz]
N = 64; 

dt = 1/fs;                 % Time interval, [s]
T = N*dt;                  % Period, [s]
times = (0:N-1)*dt;        % Time vector, [s]

xn = A*cos(2*pi*f0*times); % Sine function, [Pa] 

figure
plot(times, xn, 'bx')
title('Sine Function, xn', 'Fontsize', 14)
xlabel('Time [s]', 'Fontsize', 14); ylabel('xn [Pa]', 'Fontsize', 14)
grid on; grid minor

linSpec = fft(xn)*dt;
linSpec = linSpec.'
df = 1/N/dt
f = (0:N-1)*df; f = f.';

