function [freqVec, avgGxx] = averageGxx(timeSeries, dt, recordSize, overlap, Nrecs)

% PURPOSE:
% This function outputs the average single-sided spectral density (Gxx) of
% a given time series.

% INPUTS: 
%   timeSeries = vector of data [WU]
%   dt = time increment between samples [s]
%   recordSize = Number of points in each successive record of the time series
%   overlap = overlapping expressed as a percentage (ex: 50 for 50% overlapping)
%   Nrecs = Number of records taken, (if undefined then it is calculated
%   within function)

% OUTPUTS: 
%   freqVec = vector of frequencies for avgGxx [Hz]
%   avgGxx = average single-sided spectral density [WU^2/Hz]

% WRITTEN BY ANNA FRAZIER, SEPTEMBER 2021

%If Nrecs is not defined, then it is calculated here: 
    %The purpose for doing it this way is to only let the user set if not
    %the entire timeSeries will be used for the averaging
if exist('Nrecs', 'var') == 0
    Nrecs = length(timeSeries)/recordSize; % Number of records
    if overlap ~= 0
        Nrecs = 1+(Nrecs-1)*(1/(1-overlap));
    end
    if floor(Nrecs) ~= Nrecs
        Nrecs = floor(Nrecs);
        error('Length of the time series is not divisible by N. Choose new N.') 
    end
end

indexSpacing = recordSize*(1-overlap); % Determines spacing of each Gxx 

GxxSum = 0; % Initialize sum of Gxx 
for i = 1:Nrecs % Loops through number of records
    
    % Determines index of first/last point for this Gxx and pulls it from the data
    first = indexSpacing*(i-1)+1; 
    last = indexSpacing*i; 
    record = timeSeries(first:last);
    
    %Applies hann window to the record
%     HW = hannWindow(length(record)); 
%     if size(HW) ~= size(record)
%         HW = HW.';
%     end
%     record = HW.*record;     
    
    % Calculates spectral density of the record
    [freqVec,Gxx] = spectralDensity(record,dt,'single',0);
    
    % The code below can be uncommented to plot each Gxx for troubleshooting
%     figure('Name', 'Gxx for Single Record')
%     semilogy(freqVec, Gxx)
%     xlabel('Frequency [Hz]', 'Fontsize', 14)
%     ylabel('Gxx [WU^2/Hz]', 'Fontsize', 14)
%     title('Gxx of Single Record vs. Frequency', 'Fontsize', 14)
%     xlim([0 10000])
%     ylim([1e-10 1e-3])

    GxxSum = GxxSum + Gxx; % Adds this Gxx to the sum
end

% Divides Gxx sum by total number of records to get the average 
avgGxx = GxxSum/Nrecs;

avgGxx = avgGxx.'; %transposes this vector to match the frequency vector
end

