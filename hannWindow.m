function [HW] = hannWindow(N)

% PURPOSE:
% This function creates a Hann window function with N points. This window
% is normalized so that its rms value is 1. This means that no correction
% of spectral density is necessary!

% INPUTS: 
%   N = Number of points for window

% OUTPUTS: 
%   HW = Hann window function 

% WRITTEN BY ANNA FRAZIER, OCTOBER 2021

HW = hann(N, 'periodic'); % uses MATLAB's hann function
ms = sum(HW.*HW)/N;       % mean-square of HW
HW = HW/sqrt(ms);         % forces rms value to be 1

end

