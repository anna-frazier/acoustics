%SCRIPT TO TEST COHERENCE

%Code to generate pure tones for SoundNife testing 

f = 1000; 
fs = 100000; 
dt = 1/fs; 

d = 1; %10 second tone
t = 0:dt:d-dt; 

sineWav = sin(2*pi*f*t); 
sineWav_pi = sin(2*pi*f*t+pi/2);

figure
plot(sineWav)
hold on 
plot(sineWav_pi)
grid on 
xlim([0 300])
ylim([-2 2])

% Comparing cross spectrum functions
%[pxy, f] = cpsd(sineWav,sineWav, [], [], [], fs); 
%[freqVec, Gxy] = crossSpectrum(sineWav, sineWav, dt);

% Comparing two sine waves that are exactly the same 
[~, avgGxy] = averageGxy(sineWav, sineWav, dt, 0.1/dt, 0.5); 
[~, avgGxx] = averageGxx(sineWav, dt, 0.1/dt, 0.5); 
[freqVec, avgGyy] = averageGxx(sineWav, dt, 0.1/dt, 0.5); 
coherence = abs(avgGxy).^2./(avgGxx.*avgGyy);

figure
subplot(2,1,1)
plot(freqVec, coherence, 'LineWidth', 1)
hold on
xlim([0 10000])
xlabel('Frequency [Hz]')
ylabel('Coherence, \gamma^2')
title('Identical 1 kHz Sine Waves: T = 1 s')
grid on 
grid minor


d = 100; %10 second tone
t = 0:dt:d-dt; 

sineWav = sin(2*pi*f*t); 
sineWav_pi = sin(2*pi*f*t+pi/2);

% figure
% plot(sineWav)
% hold on 
% plot(sineWav_pi)
% grid on 
% xlim([0 300])
% ylim([-2 2])

% Comparing cross spectrum functions
%[pxy, f] = cpsd(sineWav,sineWav, [], [], [], fs); 
%[freqVec, Gxy] = crossSpectrum(sineWav, sineWav, dt);

% Comparing two sine waves that are exactly the same 
[~, avgGxy] = averageGxy(sineWav, sineWav, dt, 0.1/dt, 0.5); 
[~, avgGxx] = averageGxx(sineWav, dt, 0.1/dt, 0.5); 
[freqVec, avgGyy] = averageGxx(sineWav, dt, 0.1/dt, 0.5); 
coherence = abs(avgGxy).^2./(avgGxx.*avgGyy);

subplot(2,1,2)
plot(freqVec, coherence, 'LineWidth', 1)
hold on
xlim([0 10000])
xlabel('Frequency [Hz]')
ylabel('Coherence, \gamma^2')
title('Identical 1 kHz Sine Waves: T = 100 s')
grid on 
grid minor


% Comparing two sine waves that are pi out of phase
% [~, avgGxy] = averageGxy(sineWav, sineWav_pi, dt, 0.1/dt, 0.5); 
% [~, avgGxx] = averageGxx(sineWav, dt, 0.1/dt, 0.5); 
% [freqVec, avgGyy] = averageGxx(sineWav_pi, dt, 0.1/dt, 0.5); 
% coherence = abs(avgGxy).^2./(avgGxx.*avgGyy);

% figure
% plot(freqVec, coherence)
% hold on
% %plot(f, pxy)
% %legend('Mine', 'MATlAB')
% xlim([0 10000])
% title('Sine Waves Pi Out of Phase')