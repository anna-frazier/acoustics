%Anna Frazier
%ACS 503 - Signal Analysis
%Homework 5 - Spectrogram 

% clear variables 
% close all 

%% CONSTRUCTION OF SPECTROGRAM NOTES
% (1) Choose a record length
%     - You want this to be an integer power of two (yields the fastest
%       execution time of the fft function
% (2) Choose a time-domain window function & percentage overlap
%     - The Hann window with an overlap of 75% to 90% is good for a
%       spectrogram.
%     - This creates smoothing in the time domain. Smoothing in the
%       frequency domain is also possible with zero padding, but not as
%       common. 
% (3) For each record, apply the window function and compute the
%     single-sided spectral density.
%     - (optional) Convert values to decibels. 
%     - (optional) Drop the zero-frequency value (and perhaps a few of its
%       neighbors). 
% (4) Once each record is calculated, add it as a column vector to the
%     overall spectrogram matrix.
% (5) Use MATLAB's imagesc function to display the matrix. Add the line
%     "axis xy" to change the display such that frequency increases upward. 
%     - To use imagesc, you'll need a time vector and a frequency vector.
%       These can be two-element vectors: the mid-point time for the first
%       and last records in the time vector and the lowest and highest bin
%       frequencies in the frequency vector. (Or you can construct these
%       with all values in each of these vectors - more memory but
%       preferred)

%STOPPED AT 4.2.1 DECIBELS AND THE SPECTROGRAM

%% PART 1 & 2: Simple Spectrogram 
% Don't bother with a time-domain window, overlapping records, or
% converting to decibels.

%Generate the test signal: 
fs = 2048;            %Sampling Rate [S/s]
dt = 1/fs;            %Time increment [s]
f0 = 128;             %Frequency of Sine Wave [Hz]
T = 6;                %Length of each section of the signal [s]
N = T/dt;             %Number of samples in entire signal
Z = zeros(T/3/dt, 1); %Zeros that make up start and end of signal

sineTimeVec = (0:(N/3)-1)*dt;           %time vector for sine part of signal
timeVec = (0:N-1)*dt; timeVec = timeVec; %Time vector for entire signal

sineWave = sin(2*pi*f0*sineTimeVec); sineWave = sineWave'; %Sine wav [V]
timeSeries = [Z; sineWave; Z];                             %Entire signal [V]

recordSize = 256; %Number of samples in each record

[specMatrix, specFreqVec, specTimeVec] = mySpectrogram(timeSeries, dt, recordSize, 0.75,1);

%% PART 3: Reading in wav file

[data, fs] = audioread('T4_C5_3L_dec4a.wav'); 

[specMatrix, specFreqVec, specTimeVec] = mySpectrogram(data, 1/fs, 2048, 0.75, 1);
caxis([-130 -30])
title('Spectrogram of Plane Recording')

%% PART 4: Recording a wav file and making spectrogram 

[data, fs] = audioread('Ukulele.wav'); 
[specMatrix, specFreqVec, specTimeVec] = mySpectrogram(data, 1/fs, 4096*4, 0.75, 1);
title('Spectrogram of My Ukulele Recording')
caxis([-120 -50])
ylim([0 1000])
xlim([2 4])



