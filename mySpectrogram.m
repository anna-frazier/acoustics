function [specMatrix, specFreqVec, specTimeVec] = mySpectrogram(timeSeries, dt, recordSize, overlap, fig)

% PURPOSE:
% 

% INPUTS: 
%   timeSeries = vector of data [WU]
%   dt = time increment between samples [s]
%   recordSize = Number of samples in each record
%   Nrecs = Number of records (if undefined it is calculated in function)
%   overlap = overlapping expressed as a percentage (ex: 0.5 for 50% overlapping)
%   fig = option to plot (will plot if equal to 1)

% OUTPUTS: 
%   

% WRITTEN BY ANNA FRAZIER, OCTOBER 2021

if exist('Nrecs', 'var') == 0
    Nrecs = length(timeSeries)/recordSize; % Number of records
    if overlap ~= 0
        Nrecs = 1+(Nrecs-1)*(1/(1-overlap));
    end
    if floor(Nrecs) ~= Nrecs
        Nrecs = floor(Nrecs);
        disp('Note: Length of the time series is not divisible by N.') 
    end
end

indexSpacing = recordSize*(1-overlap);

specMatrix = []; 
for i = 1:Nrecs
    first = indexSpacing*(i-1)+1;
    last = indexSpacing*i; 
    record = timeSeries(first:last);
    HW = hannWindow(length(record));
    record = HW.*record; 
    [freqVec, specDens] = spectralDensity(record, dt, 'single', 0);
    specDens = 10*log10(specDens); %Converts it to dB
    specMatrix(:,i) = specDens;
end

specFreqVec = freqVec;
specTimeVec = [(dt/2):dt:(Nrecs*indexSpacing*dt-(dt/2))];

if fig == 1
    figure('Name', 'Spectrogram')
    imagesc(specTimeVec, specFreqVec, specMatrix)
    axis xy
    title('Spectrogram of Time Series')
    xlabel('Time [s]')
    ylabel('Frequency [Hz]')   
    grid on 
    grid minor
    h=colorbar;
    colormap(turbo);
    h2=get(h,'YLabel');
    set(h2,'string','Gxx [dB re 1 Pa^2/Hz]')
    
end


end

