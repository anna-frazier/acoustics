function [freqVec, avgGxy] = averageGxy(timeSeries1, timeSeries2, dt, recordSize, overlap, Nrecs)

% PURPOSE:
% This function outputs the average cross-spectral density (Gxy) of
% 2 given time series.

% INPUTS: 
%   timeSeries1 = 1st time series [WU]
%   timeSeries2 = 2nd time series [WU]
%   dt = time increment between samples [s]
%   recordSize = Number of points in each successive record of the time series
%   overlap = overlapping expressed as a percentage (ex: 50 for 50% overlapping)
%   Nrecs = Number of records taken, (if undefined then it is calculated
%   within function)

% OUTPUTS: 
%   freqVec = vector of frequencies for avgGxx [Hz]
%   avgGxy = average cross-spectral density [WU^2/Hz]

% WRITTEN BY ANNA FRAZIER, OCTOBER 2021

%If Nrecs is not defined, then it is calculated here: 
    %The purpose for doing it this way is to only let the user set if not
    %the entire timeSeries will be used for the averaging
if exist('Nrecs', 'var') == 0
    Nrecs = length(timeSeries1)/recordSize; % Number of records
    if overlap ~= 0
        Nrecs = 1+(Nrecs-1)*(1/(1-overlap));
    end
    if floor(Nrecs) ~= Nrecs
        Nrecs = floor(Nrecs);
        error('Length of the time series is not divisible by N. Choose new N.') 
    end
end

indexSpacing = recordSize*(1-overlap); % Determines spacing of each Gxx 

GxySum = 0; % Initialize sum of Gxx 
for i = 1:Nrecs % Loops through number of records
    
    % Determines index of first/last point for this Gxx and pulls it from the data
    first = indexSpacing*(i-1)+1; 
    last = indexSpacing*i; 
    record1 = timeSeries1(first:last);
    record2 = timeSeries2(first:last);
    
    %Applies hann window to the record
%     HW = hannWindow(length(record1)); 
%     if size(HW) ~= size(record1)
%         HW = HW.';
%     end
%     record1 = HW.*record1;
%     record2 = HW.*record2;
    
    % Calculates spectral density of the record
    [freqVec,Gxy] = crossSpectrum(record1,record2,dt, 'single');
    
    % The code below can be uncommented to plot each Gxy for troubleshooting
%     figure('Name', 'Gxy for Single Record')
%     semilogy(freqVec, Gxy)
%     xlabel('Frequency [Hz]', 'Fontsize', 14)
%     ylabel('Gxy [WU^2/Hz]', 'Fontsize', 14)
%     title('Gxy of Single Record vs. Frequency', 'Fontsize', 14)
%     xlim([0 10000])
%     ylim([1e-10 1e-3])

    GxySum = GxySum + Gxy; % Adds this Gxx to the sum
end

% Divides Gxx sum by total number of records to get the average 
avgGxy = GxySum/Nrecs;

avgGxy = avgGxy.'; %transposes this vector to match the frequency vector

end