function [b, a] = expAvgFilt(Tc, fs)

% PURPOSE:
% This function outputs the time-domain coefficients for an
% exponential-averaging filter

% INPUTS: 
%   Tc = time constant [s]
%   fs = sampling frequency [Hz] 

% OUTPUTS: 
%   [b, a] = time-domain coefficients

% WRITTEN BY ANNA FRAZIER, NOVEMBER 2021
dt = 1/fs; 
alpha = dt/Tc; 

% Compute b and a coefficients
b = [0, alpha]; 
a = [1, alpha-1];
end