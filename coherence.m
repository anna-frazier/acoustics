function [freqVec, c] = coherence(timeSeries1, timeSeries2, dt, recordSize, overlap, Nrecs)

% PURPOSE:
% This function outputs the average single-sided spectral density (Gxx) of
% a given time series.

% INPUTS: 
%   timeSeries1 = 1st time series [WU]
%   timeSeries2 = 2nd time series [WU]
%   dt = time increment between samples [s]
%   recordSize = Number of points in each successive record of the time series
%   overlap = overlapping expressed as a percentage (ex: 50 for 50% overlapping)
%   Nrecs = Number of records taken, (if undefined then it is calculated
%   within function)

% OUTPUTS: 
%   freqVec = vector of frequencies for avgGxx [Hz]
%   c = Magnitude Squared Coherence 

% WRITTEN BY ANNA FRAZIER, SEPTEMBER 2021

%If Nrecs is not defined, then it is calculated here: 
    %The purpose for doing it this way is to only let the user set if not
    %the entire timeSeries will be used for the averaging
if exist('Nrecs', 'var') == 0
    Nrecs = length(timeSeries1)/recordSize; % Number of records
    if overlap ~= 0
        Nrecs = 1+(Nrecs-1)*(1/(1-overlap));
    end
    if floor(Nrecs) ~= Nrecs
        Nrecs = floor(Nrecs);
        error('Length of the time series is not divisible by N. Choose new N.') 
    end
end

[~, avgGxy] = averageGxy(timeSeries1, timeSeries2, dt, recordSize, overlap, Nrecs); 
[~, avgGxx] = averageGxx(timeSeries1, dt, recordSize, overlap, Nrecs); 
[freqVec, avgGyy] = averageGxx(timeSeries2, dt, recordSize, overlap, Nrecs); 

c = (abs(avgGxy).^2)./(avgGxx.*avgGyy); 
end 
