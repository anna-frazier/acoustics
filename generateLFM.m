function [timeVec, LFMsignal] = generateLFM(f1, f2, Tp, fs)

B = (f2-f1)/Tp; % Chirp Rate [Hz/s]

dt = 1/fs; 

timeVec = 0:dt:Tp-dt; 

phase = 2*pi*((f1*timeVec)+(0.5*B*timeVec.^2)); 

LFMsignal = sin(phase);

% Converts them to row vectors:
timeVec = timeVec'; 
LFMsignal = LFMsignal';

end 
