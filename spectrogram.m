function [specMatrix, specFreqVec, specTimeVec] = spectrogram(timeSeries, dt, recordSize)

% PURPOSE:
% 

% INPUTS: 
%   timeSeries = vector of data [WU]
%   dt = time increment between samples [s]
%   recordSize = Number of samples in each record
%   Nrecs = Number of records (if undefined it is calculated in function)

% OUTPUTS: 
%   

% WRITTEN BY ANNA FRAZIER, OCTOBER 2021

if exist('Nrecs', 'var') == 0
    Nrecs = length(timeSeries)/Npoints; % Number of records
    if floor(Nrecs) ~= Nrcs
        error('Length of the time series is not divisible by N. Choose new N.') 
    end
end



for i = 1:Nrecs
    record = timeSeries((recordSize*(i-1)+1):(recordSize*i)); 
    [freqVec, specDens] = spectralDensity(record, dt, 'single', 0);
end

end

