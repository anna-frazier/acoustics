function [timeVec, Rxy] = crossCorrelation(timeSeries1, timeSeries2, fs, reorder)

% PURPOSE:
% This function outputs the a

% INPUTS: 
%   timeSeries1 = 1st vector of data [WU]
%   timeSeries2 = 2nd vector of data [WU]
%   fs = sample rate [S/s]
%   reorder = if 1, then the time series will be adjusted to run from (-T/2+dt) to (+T/2)

% OUTPUTS: 
%   lagVec = Lag time vector [s]
%   Rxy = cross-correlation [unitless]

% WRITTEN BY ANNA FRAZIER, NOVEMBER 2021

if length(timeSeries1)~=length(timeSeries2)
    error('The two time series are not the same length!')
end

N = length(timeSeries1);
dt = 1/fs; 
T = N*dt; 
timeVec = (0:N-1)*dt; timeVec = timeVec'; 

[freqVec, Sxy] = crossSpectrum(timeSeries1,timeSeries2,dt,'double');

% Compute Rxy from Sxy using ifft: 
Rxy = ifft(Sxy)/dt; 

if reorder == 1
    if rem(length(timeVec),2) == 0 %If Rxy has even number of points
        timeVec = ((-N/2+1):(N/2))*dt; timeVec = timeVec'; 
        Rxy = [Rxy((N/2+1):N) Rxy(1:N/2)];
    else 
        
    end

end
