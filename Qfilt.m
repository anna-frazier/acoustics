function [b, a] = Qfilt(Q, fc, fs)

% PURPOSE:
% This function outputs the time-domain coefficients for a constant Q-filter

% INPUTS: 
%   Q [unitless]
%   fc = center frequency [Hz]
%   fs = sampling frequency [Hz] 

% OUTPUTS: 
%   [b, a] = time-domain coefficients

% WRITTEN BY ANNA FRAZIER, NOVEMBER 2021

omega = 2*pi*fc; 
alpha = 2*fs/omega; 

% Compute b and a coefficients
b = [alpha/Q, 0, -alpha/Q]; 
a = [1+(alpha/Q)+alpha^2, 2-(2*alpha^2), 1-(alpha/Q)+alpha^2];

end