function [timeVec, CWsignal] = generateCW(f, Tp, fs)

dt = 1/fs; 
timeVec = 0:dt:(Tp-dt); 

CWsignal = sin(2*pi*f*timeVec); 

end
