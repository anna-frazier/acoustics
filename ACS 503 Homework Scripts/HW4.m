%Anna Frazier 
%ACS 503 - Signal Analysis 
%Homework 4 - Averaging

clear variables 
close all 

%% SECTION 1, PART 1

% Create Sine Wave: 
N = 1000;                           % Lenth of time series
fs = 96000;                         % Sample rate, [Hz]
dt = 1/fs;                          % Time interval, [s]
freq = 1000;                        % Frequency of sine wav, [Hz]

timeVec = (0:N-1)*dt;               % Time vector, [s]
sineWave = sin(2*pi*freq*timeVec);  % Time series, [WU]

% Test Average Gxx Function on Sine Wave: 
[freqVec, avgGxx] = averageGxx(sineWave, dt, 10);  % Average Gxx, [WU^2/Hz]

%% SECTION A, PART 2 & 3

clear variables 
close all 

filename = 'S_plus_N_20.wav';       % Name of our wave file with data

[yy, fs] = audioread(filename);     % yy = data [WU], fs = sample rate [Hz]

[avgFreqVec, avgGxx] = averageGxx(yy, 1/fs, 1024, 200);   % Average Gxx, [WU^2/Hz]

figure('Name', 'Part 3: Average Gxx')
plot(avgFreqVec, avgGxx)
xlabel('Frequency [Hz]', 'Fontsize', 14)
ylabel('Average Gxx [WU^2/Hz]', 'Fontsize', 14)
title('Average Gxx vs. Frequency', 'Fontsize', 14)

%Peak Value = 1.58e-5 at 375 Hz

%% SECTION A, PART 4

[freqVec,Gxx] = spectralDensity(yy(1:(200*1024)),1/fs,'single',0);

figure('Name', 'Part 4')
plot(freqVec, Gxx, 'LineWidth', 1.5)
hold on 
plot(avgFreqVec, avgGxx, 'LineWidth', 1.5)
xlabel('Frequency [Hz]', 'Fontsize', 14)
ylabel('Singled-Sided Spectral Density [WU^2/Hz]', 'Fontsize', 14)
legend('Not Averaged, Nrecs = 1', 'Averaged, Nrecs = 200')

rms_avgGxx = rms(avgGxx)
rms_Gxx = rms(Gxx)

%% SECTION B, PART 1

clear variables 
close all 

filename = 'P_plus_N_10.wav';       % Name of our wave file with data

[yy, fs] = audioread(filename);     % yy = data [WU], fs = sample rate [Hz]

% figure('Name', 'Part 1: Time Series')
% plot(yy)
% xlabel('Samples')
% ylabel('WU')
% title('Part 1: Time Series')

%% SECTION B, PART 2

[avgFreqVec, avgGxx] = averageGxx(yy, 1/fs, 1024, 200);   % Average Gxx, [WU^2/Hz]

figure('Name', 'Part 2: Average Gxx')
plot(avgFreqVec, avgGxx)
xlabel('Frequency [Hz]', 'Fontsize', 14)
ylabel('Average Gxx [WU^2/Hz]', 'Fontsize', 14)
title('Average Gxx vs. Frequency', 'Fontsize', 14)

%% SECTION B, PART 3

GxxSum = 0;
Nrecs = 200; 
N = 1024; 
Nrepeat = 1111; 

for i = 1:Nrecs
    first = (Nrepeat*(i-1)+1); 
    last = first+N-1; 
    [freqVec,Gxx] = spectralDensity(yy(first:last),1/fs,'single',0);
    GxxSum = GxxSum + Gxx; 
end

avgGxx = GxxSum/Nrecs;
avgGxx = avgGxx.'; %transposes this vector to match the frequency vector

figure('Name', 'Part 3: Synchronized Averaged Gxx')
plot(freqVec, avgGxx)
xlabel('Frequency [Hz]', 'Fontsize', 14)
ylabel('Averaged Gxx [WU^2/Hz]', 'Fontsize', 14)
title('Synchronized Averaged Gxx', 'Fontsize', 12)

%% SECTION B, PART 4

Xsum = 0; 
Nrecs = 200; 
N = 1024; 
Nrepeat = 1111; 

%Calculate Synchronized Xavg: 
for i = 1:Nrecs
    first = (Nrepeat*(i-1)+1); 
    last = first+N-1; 
    [freqVec,linSpec] = linearSpectrum(yy(first:last),1/fs,0);
    Xsum = Xsum + linSpec; 
end

avgX = Xsum/Nrecs; avgX = avgX.';

specDens = abs(avgX).^2/(length(avgX)/fs); 
for i = 1:length(specDens)
    if i>1 & i<(N/2+1)
        specDens(i) = specDens(i)*2; 
    end
end
specDens_sync = specDens(1:(N/2+1)); 
freqVec_sync = freqVec(1:(N/2+1));

figure('Name', 'Part 4: Synchronized, Vector Averaged Gxx')
plot(freqVec_sync, specDens_sync)
xlabel('Frequency [Hz]', 'Fontsize', 14)
ylabel('Averaged Gxx [WU^2/Hz]', 'Fontsize', 14)
title('Part 4: Synchronized Vector Averaged Gxx', 'Fontsize', 12)

%Calculate Unsynchronized Xavg: 

Xsum = 0; 
Nrecs = 200; 
N = 1024; 

for i = 1:Nrecs
    [freqVec,linSpec] = linearSpectrum(yy(((i-1)*N+1):N*i),1/fs,0);
    Xsum = Xsum + linSpec; 
end

avgX = Xsum/Nrecs; avgX = avgX.';
specDens = abs(avgX).^2/(length(avgX)/fs); 
for i = 1:length(specDens)
    if i>1 & i<(N/2+1)
        specDens(i) = specDens(i)*2; 
    end
end
specDens_unsync = specDens(1:(N/2+1)); 
freqVec_unsync = freqVec(1:(N/2+1));

figure('Name', 'Part 4: Unsynchronized, Vector Averaged Gxx')
plot(freqVec_unsync, specDens_unsync)
xlabel('Frequency [Hz]', 'Fontsize', 14)
ylabel('Averaged Gxx [WU^2/Hz]', 'Fontsize', 14)
title('Part 4: Unsynchronized Vector Averaged Gxx', 'Fontsize', 12)

%% SECTION B, PART 5

xsum = 0; 
Nrecs = 200; 
N = 1024; 
Nrepeat = 1111; 

for i = 1:Nrecs
    first = (Nrepeat*(i-1)+1); 
    last = first+N-1; 
    x = yy(first:last);
    xsum = xsum+x;
    
    if i == 1
        figure
        plot(xsum)
        ylabel('Individual Time Record [WU]')
        xlim([0 1024])
        xlabel('Samples')
        title('Part 5: Time Series of Individual Record', 'Fontsize', 12)
    end
    
end

xavg = xsum/Nrecs;

figure('Name', 'Part 5: Synchronized Time Average')
plot(xavg)
xlabel('Samples', 'Fontsize', 14)
ylabel('Time Average [WU]', 'Fontsize', 14)
xlim([0 1024])
title('Part 5: Synchronized Time Average', 'Fontsize', 12)

%% SECTION B, PART 6

[freqVec,specDens] = spectralDensity(xavg,1/fs,'single',1)


