function [freqVec,specDens] = spectralDensity(timeSeries,dt,outputType,fig)

% PURPOSE:
% This function outputs the spectral density (single sided or double sided) 
% for a given time series.

% INPUTS: 
%   ftimeSeries = vector of data [WU]
%   dt = time increment between samples [s]
%   outputType = 'single' (Gxx) or 'double' (Sxx)
%   fig = option to plot (will plot if equal to 1)

% OUTPUTS: 
%   freqVec = frequency vector of spectral density [Hz]
%   specDens = spectral density [WU^2*s]

% WRITTEN BY ANNA FRAZIER, SEPTEMBER 2021

[freqVec, linSpec] = linearSpectrum(timeSeries, dt, fig);
N = length(timeSeries); 
T = N*dt;
fs = 1/dt; 
specDens = abs(linSpec).^2/T; %specDens = specDens';

if outputType == 'double'
    for i = 1:length(freqVec)
        if freqVec(i)>(fs/2)
            freqVec(i) = freqVec(i)-fs; 
        end
    end
end 

if outputType == 'single'
    for i = 1:length(specDens)
        if i>1 & i<(N/2+1)
            specDens(i) = specDens(i)*2; 
        end
    end
    specDens = specDens(1:(N/2+1)); 
    freqVec = freqVec(1:(N/2+1));
end

if fig == 1
    figure('Name', 'Spectral Density')
    plot(freqVec, specDens)
    xlabel('Frequency [Hz]', 'FontSize', 12)
    ylabel('Spectral Density [WU^2*s]', 'FontSize', 12)
    switch outputType
        case 'single'
            title('Single-Sided Spectral Density, Gxx')
        case 'double'
            title('Double-Sided Spectral Density, Sxx')
    end
    grid on 
    grid minor
end

end

