%Anna Frazier
%ACS 503
%Homework 3, Due 9/11/2021

%% TEST

x = [-4, -9, -6, 1, 1, -3, 6, -2];
fs = 1000; 
dt = 1/fs; 
N = 8; 
T = N*dt; 
df = 1/T; 

Gxx = [0.0320, 0.0821, 0.0325, 0.0249, 0.0125]; 

val2 = rms(x); 
val1 = rms(Gxx); 


%% PART 1

clear all; close all; clc;

%Read data from csv file: 
filename = 'TRAC1_noise_time.csv';
T = readtable(filename);

timeVec = T.Var1;     % [s]
voltageData = T.Var2; % [V]

%Plot data from csv file: 
figure('Name', 'Part 1: Noise Data')
plot(timeVec, voltageData); 
title('Part 1: Time Series Data')
xlabel('Time [s]')
ylabel('Voltage [V]')
grid on 
grid minor

%Determine N and dt: 
N = length(timeVec); 
dt = timeVec(2)-timeVec(1);

%Find single-sided spectral density, Gxx, and plot as a function of frequency:
[freqVec_Gxx,specDens_Gxx] = spectralDensity(voltageData,dt,'single',1)

%Find the mean square of xn and the sum of all Sxx values times df:
[freqVec_Sxx,specDens_Sxx] = spectralDensity(voltageData,dt,'double',0)
mean(voltageData.^2)
N = length(specDens_Sxx);
df = 1/N/dt; 
sum(specDens_Sxx)*df

%Play the file:
%sound(voltageData/3,1/dt,16); % play sound

val1 = rms(specDens_Gxx)
val2 = rms(voltageData)

%% PART 2

clear all; close all; clc;

%Read data from csv file: 
filename = 'TRAC3_sin100_time.csv';
T = readtable(filename);

timeVec = T.Var1;     % [s]
voltageData = T.Var2; % [V]

%Plot data from csv file: 
figure('Name', 'Part 2: Sine Wave Data')
plot(timeVec, voltageData); 
title('Part 1: Time Series Data')
xlabel('Time [s]')
ylabel('Voltage [V]')
grid on 
grid minor

%Determine N and dt: 
N = length(timeVec); 
dt = timeVec(2)-timeVec(1);

%Find the double-sided spectral density, Sxx: 
[freqVec_Sxx,specDens_Sxx] = spectralDensity(voltageData,dt,'double',0)

%Find the single-sided spectral density, Gxx, and plot as a function of frequency: 
[freqVec_Gxx,specDens_Gxx] = spectralDensity(voltageData,dt,'single',1)
    %Frequency of peak is at 100 Hz. 
    %Units of Gxx: [V^2*s] or [V^2/Hz]
    
val1 = rms(specDens_Gxx)
val2 = rms(voltageData)
    %What is the relationship between these? 
    
%Play the file:
sound(voltageData/1.5,1/dt,16); % play sound

%% PART 3

clear all; close all; clc;

nbits = 16;     % number of bits per sample [bits/S]
nchannels = 1;  % number of channels
duration = 10;   % duration of recording [s]   
fs = 44100;     % sampling frequency [Hz]

objr = audiorecorder(fs, nbits, nchannels, 1); % set up objr

disp('Recording')
record(objr, duration); % begin recording

pause

micData = getaudiodata(objr); % extract microphone data from objr

('Playing')
sound(micData, fs, nbits);

dt = 1/fs; 
N = length(micData);
timeVec = (0:N-1)*dt; timeVec = timeVec.'

figure
plot(timeVec, micData)
title('Part 3: Time-Domain Signal of Microphone Recording')
xlabel('Time [s]')
ylabel('Signal [WU]')

[freqVec,specDens] = spectralDensity(micData,dt,'single',1);
xlim([0 10000])
title('Part 3: Single-Sided Spectral Densisty')