%Anna Frazier
%ACS 503
%Homework 2, Due 9/4/2021

%% Creating white noise linear spectrum

N = 48000;      % [S], use only even numbers
X = zeros(N,1); % [WU]

%m=0 and m=N/2, real
X(1) = 1; 
X(N/2+1) = 1;

for i = 2:(N/2)
    theta = rand(1)*2*pi; 
    X(i) = cos(theta) + 1i*sin(theta); 
end

for i = (N/2+2):N
    X(i) = conj(X(N-i+2)); 
end

%X = ones(N,1);

%% Creating pink noise linear spectrum 

% N = 48000; 
% X = zeros(N,1); 
% 
% X(1) = 0; 
% X(N/2+1) = 1/sqrt(N/2); 
% 
% for i = 2:(N/2)
%     theta = rand(1)*2*pi; 
%     X(i) = (cos(theta) + 1i*sin(theta))*(1/sqrt(i-1)); 
% end
% 
% for i = (N/2+2):N
%     X(i) = conj(X(N-i+2)); 
% end
%% Generate time series from linear spectrum

fs = 24000;           % sample rate [S/s]
dt = 1/fs;            % time increment [s]
T = N*dt;             % period [s]
df = 1/T;             % frequency increment [Hz]

timeVec = (0:N-1)*dt; % time vector [s]
x = ifft(X)/dt;       % time series [WU*s]
% 
% %See random noise
% figure
% plot(timeVec, x)
% title('Time-Domain Signal', 'FontSize', 12)
% xlabel('Time [s]', 'FontSize', 12)
% ylabel('Noise Signal [WU]', 'FontSize', 12)
% grid on 
% grid minor

%% Use these to check the phase distribution of the linear spectrum

% figure
% scatter(abs(X),angle(X)+pi)
% 
% figure
% hist(angle(X)+pi)

%% 

% m = mean(x);          % mean of time series [WU]
% ms = mean(x.^2);      % mean-square of time series [WU^2]
% rms = sqrt(ms);       % root-mean-square of time series [WU]
% 
% peakVal = max(x);     % peak value of time series [WU]
% CF = peakVal/rms;     % ratio of peak value to root-mean-square [WU/WU]
% 
% s = sin(pi*timeVec);  % sine wave signal [WU]
% ms_s = mean(s.^2);    % mean-square of the sine wave [WU^2]
% rms_s = sqrt(ms_s)    % root-mean-square of the sine wav [WU]
% CF_s = max(s)/rms_s;  % ratio of peak value to root-mean-square for sine wave [WU/WU]

%% Play and record signal

nbits = 16;     % number of bits per sample [bits/S]
nchannels = 1;  % number of channels
length = 5;     % duration of recording [s]     
x = x/10;       % time series scaled by 1/10 [WU]
x = [x;x];      % scaled time series repeated twice [WU]

objr = audiorecorder(fs, nbits, nchannels, 1); % set up objr
record(objr, length); % begin recording

pause(0.1) % this pause prevents clicking noise in sound being played: 

sound(x,fs,nbits); % play sound

pause(length) %pause to allow recording to finish:

play(objr) %play recording

micdata = getaudiodata(objr); % extract microphone data from objr

%% Analyze signal recording

fs = 24000;  % sample rate [S/s]
dt = 1/fs;   % time interval [s]
N = 48000;   % number of samples 
T = dt*N;    % period [s]
df = 1/T;    % frequency interval [Hz]

%Select 48000 samples from within micdata:
first = 30000; 
last = first+N-1;
samples = micdata(first:last); 

%Find the mean and subtract from our data:
ms = mean(samples); 
samples = samples - ms; 

%Calculate our initial linear spectrum and frequency vector: 
X = fft(samples)*dt;        % [WU*s]
freqVec = [(0:N-1).*df]';  % [Hz]

%Use logical indicing to shift the frequency vector and only keep positive values: 
freqVec(freqVec>fs/2) = freqVec(freqVec>fs/2)-fs; %
X = [X(freqVec>0)];
freqVec = [freqVec(freqVec>0)]; 

%Plot our linear spectrum: 
figure
plot(freqVec, abs(X))
xlim([0 8000]) %Limited to 8000 Hz due to computer speaker capabilities
xlabel('Frequency [Hz]', 'FontSize', 12)
ylabel('Linear Spectrum [WU/Hz]', 'FontSize', 12)
title('Part 5: Linear Spectrum of White Noise')
grid on 
grid minor

